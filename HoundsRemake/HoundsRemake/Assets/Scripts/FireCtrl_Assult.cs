using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireCtrl_Assult : FireCtrl
{
    BulletLine[] _bulletLineArray;

    protected override void Start()
    {
        //base.Start();
        firePos = this.transform;
        _bulletLineArray = GetComponentsInChildren<BulletLine>();

        for (int i = 0; i < _bulletLineArray.Length; i++)
        {
            _bulletLineArray[i].damage = 15;
        }
        referencedCoolTime = 0.5f;
    }

    public override void Fire()
    {
        if (_coolTime < 0)
        {
            _coolTime = referencedCoolTime;
            StartCoroutine(AssultRapidFire());
        }
    }

    IEnumerator AssultRapidFire()
    {
        for (int i = 0; i < 3; i++)
        {
            yield return new WaitForSeconds(0.1f);
            CameraManager._instance.ShakeCam();
            //_bulletLine.CallBulletLine();
             _bulletLineArray[ Random.Range(0, _bulletLineArray.Length - 1)].CallBulletLine();
            StartCoroutine(ShowMuzzleFlash());

            ///이건 여기에 왜 붙어있지? 
            _coolTime = referencedCoolTime;
        }
    }
}
