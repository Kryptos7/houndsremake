using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReviveGauge : MonoBehaviour
{
    Transform mainCam;

    private void Start()
    {
        mainCam = Camera.main.transform;
    }


    void LateUpdate()
    {
        transform.LookAt(transform.position + mainCam.rotation * Vector3.forward,
            mainCam.rotation * Vector3.up);
    }
}
