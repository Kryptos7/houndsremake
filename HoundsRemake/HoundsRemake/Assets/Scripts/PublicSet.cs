using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PublicSet : MonoBehaviour
{
    public static readonly string EnemyTag = "Enemy";
    public static readonly string PlayerTag = "Player";
    public static readonly string GroundTag = "Ground";
    public static readonly string DeadBody = "DeadBody";
    public static readonly string GenPointTag = "GenPoint";

    public static bool SwatEntry = false;
    public static bool SisterEntry = false;
    public static bool WitchEntry = false;

}
