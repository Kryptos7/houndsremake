using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletLine : MonoBehaviour
{
    Transform tr;
    LineRenderer line;
    RaycastHit hit;

    Transform _myRoot;

    /// <summary>
    /// 동료의 경우에는 공격전 특수공격 여부를 판단해야 한다.
    /// </summary>
    FollowerBehave _floowBehave;


    /// <summary>
    /// 대미지는 각 무기에서 삽입해주도록 한다.
    /// </summary>
    public int damage;

    // Start is called before the first frame update
    void Start()
    {
        tr = GetComponent<Transform>();
        line = GetComponent<LineRenderer>();
        _myRoot = transform.root;

        ///NULL이라는 얘기는 자연히 동료가 아니라 주인공이라는 얘기이다.
        _floowBehave = _myRoot.GetComponent<FollowerBehave>();

        line.useWorldSpace = false;
        line.enabled = false;
        ///시작폭과 종료폭
        line.SetWidth(0.3f, 0.01f);
    }

    /*
    // Update is called once per frame
    void Update()
    {
        Ray ray = new Ray(tr.position + Vector3.up* 0.02f, tr.forward);

        if(Input.GetMouseButton(0))
        {
            line.SetPosition(0, tr.InverseTransformPoint(ray.origin));

            if(Physics.Raycast(ray, out hit, 100f))
            {
                line.SetPosition(1, tr.InverseTransformPoint(hit.point));
            }
            else
            {
                line.SetPosition(1, tr.InverseTransformPoint(ray.GetPoint(100f)));
            }

            StartCoroutine(this.ShowBulletLine());
        }
    }
    */



   
    public void CallBulletLine()
    {
        Ray ray = new Ray(tr.position + Vector3.up * 0.02f, tr.forward);

        line.SetPosition(0, tr.InverseTransformPoint(ray.origin));

        if (Physics.Raycast(ray, out hit, 15f))
        {
            if (hit.collider.CompareTag(PublicSet.EnemyTag))
            {
                object[] _params = new object[2];
                _params[0] = _myRoot.name;
                _params[1] = damage;

                ///NULL이 아니라는 얘기는 동료라는 얘기다.
                if(_floowBehave != null)
                {
                    _floowBehave.CheckThrowAction();
                }

                hit.collider.SendMessage("AddDamage", _params, SendMessageOptions.DontRequireReceiver);
                //Debug.Log("적을 맞췄음");
            }
            line.SetPosition(1, tr.InverseTransformPoint(hit.point));
        }
        else
        {
            line.SetPosition(1, tr.InverseTransformPoint(ray.GetPoint(15f)));
        }

        StartCoroutine(this.ShowBulletLine());
        
    }
     


    IEnumerator ShowBulletLine()
    {
        line.enabled = true;
        yield return new WaitForSeconds(Random.Range(0.01f, 0.1f));
        line.enabled = false;
    }
}
