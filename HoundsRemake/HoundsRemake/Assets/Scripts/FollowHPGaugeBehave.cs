using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;



public class FollowHPGaugeBehave : MonoBehaviour
{
    [SerializeField]
    Image _hpBar;


    //자신이 따라가야 될 캐릭터
    [SerializeField]
    Transform _target;

    /// <summary>
    /// 별도로 객체를 생성하여 주도록 한다.
    /// </summary>
    /// <param name="hp"></param>
    public void SetGauge(float hp)
    {
        _hpBar.fillAmount = hp;
    }


    public void Update()
    {
        if (_target.gameObject.activeInHierarchy == false)
        {
            this.gameObject.SetActive(false);
            return;
        }

        transform.position = _target.position + new Vector3(0,0.1f,0);
    }

}
