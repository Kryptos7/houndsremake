using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class JoyStickRotate : JoyStickMovement
{
    public override void OnDrag(PointerEventData ped)
    {
        Vector2 pos;
        if (RectTransformUtility.ScreenPointToLocalPointInRectangle(bgImg.rectTransform,
            ped.position,
            ped.pressEventCamera,
            out pos))
        {
            pos.x = (pos.x / bgImg.rectTransform.sizeDelta.x);
            pos.y = (pos.y / bgImg.rectTransform.sizeDelta.y);

            inputVector = new Vector3(pos.x * 2 + 1, 0, pos.y * 2 - 1);
            inputVector = (inputVector.magnitude > 1.0f) ? inputVector.normalized : inputVector;

            /// 어디까지 벗어날것인가를 결정 2가 되면 백그라운드 바깥까지 벗어나고
            /// 3이 되면 거의 원주 바깥까지만 벗어난다.
            joyStickImage.rectTransform.anchoredPosition =
                new Vector3(inputVector.x * (bgImg.rectTransform.sizeDelta.x / 3),
                    inputVector.z * (bgImg.rectTransform.sizeDelta.y / 3)
                    );

            RotateOffsetBehave._instance.SetRotationOffset(inputVector);
            CamPivotBehave._instance.SetRotationOffset(inputVector);

        }
    }

}
