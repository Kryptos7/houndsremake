using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireCtrl : MonoBehaviour
{
    protected Transform firePos;

    [SerializeField]
    protected MeshRenderer muzzleFlash;

    /// <summary>
    /// 샷건은 별도로 선언하도록 한다. 혹시라도 나중에 나중에 미니건이 나온다면, 그것도 별도로 선언하도록 한다.
    /// </summary>
    [SerializeField]
    protected BulletLine _bulletLine;

    /// <summary>
    /// 1초마다 한방씩 나갈리는 없지만, 일단은 기본값이 이렇다. 나중에 스나이퍼같은게 나온다면 1초보다 길어질수도 있겠지.
    /// </summary>
    protected float _coolTime = 1f;

    /// <summary>
    /// 참조만을 위한 쿨타임. 초기값을 집어넣는 용도 외에는 사용하지 말것. 각 무기마다 다르기에 readonly로 지정하지 않았다.
    /// </summary>
    protected float referencedCoolTime;

    virtual protected void Start()
    {
        firePos = this.transform;
        referencedCoolTime = _coolTime;
        _bulletLine = GetComponentInChildren<BulletLine>();
    }

    virtual protected void Update()
    {
        _coolTime -= Time.deltaTime;

        if(_coolTime < 0)
        {
            
            /*
            //Debug.DrawLine(firePos.position, firePos.forward * 10f, Color.green);
            if (Input.GetMouseButton(0))
            {
                Fire();
            }
            */

            /*
                RaycastHit hit;
                if (Physics.Raycast(firePos.position, firePos.forward, out hit, 10f))
                {
                    if (hit.collider.CompareTag(PublicSet.EnemyTag))
                    {
                        ///일단은 해두도록 하자.
                        
                        object[] _params = new object[2];
                        _params[0] = hit.point;
                        _params[1] = 20;
                        ///SendMessage
                        
                    }
                }
                */
        }
    }


    virtual public void Fire()
    {
        if(_coolTime < 0)
        {
            _bulletLine.CallBulletLine();
            _coolTime = referencedCoolTime;
            StartCoroutine(ShowMuzzleFlash());
        }
    }


    virtual protected IEnumerator ShowMuzzleFlash()
    {
        float scale = Random.Range(1f, 2f);
        muzzleFlash.transform.localScale = Vector3.one * scale;

        Quaternion rot = Quaternion.Euler(Random.Range(0, 360), Random.Range(0, 360), Random.Range(0, 360));
        muzzleFlash.transform.localRotation = rot;

        muzzleFlash.enabled = true;

        yield return new WaitForSeconds(Random.Range(0.03f, 0.05f));
        muzzleFlash.enabled = false;

    }
}
