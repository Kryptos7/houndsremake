using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;


public class JoyStickMovement : MonoBehaviour,IDragHandler, IPointerUpHandler, IPointerDownHandler
{
    [SerializeField]
    protected Image bgImg;
    [SerializeField]
    protected Image joyStickImage;
    protected Vector3 inputVector;


    protected void Start()
    {
        bgImg = GetComponent<Image>();
        joyStickImage = transform.GetChild(0).GetComponent<Image>();
    }

    public virtual void OnDrag(PointerEventData ped)
    {
        Vector2 pos;
        if (RectTransformUtility.ScreenPointToLocalPointInRectangle(bgImg.rectTransform,
            ped.position,
            ped.pressEventCamera,
            out pos))
        {
            pos.x = (pos.x / bgImg.rectTransform.sizeDelta.x);
            pos.y = (pos.y / bgImg.rectTransform.sizeDelta.y);

            inputVector = new Vector3(pos.x * 2, 0, pos.y * 2);
            inputVector = (inputVector.magnitude > 1.0f) ? inputVector.normalized : inputVector;

            /// 어디까지 벗어날것인가를 결정 2가 되면 백그라운드 바깥까지 벗어나고
            /// 3이 되면 거의 원주 바깥까지만 벗어난다.
            joyStickImage.rectTransform.anchoredPosition =
                new Vector3(inputVector.x * (bgImg.rectTransform.sizeDelta.x / 3),
                    inputVector.z * (bgImg.rectTransform.sizeDelta.y / 3)
                    );

            //Debug.Log(inputVector);

            PlayerBehave._instance.SetDirection(inputVector);
        }
    }

    public virtual void OnPointerDown(PointerEventData ped)
    {
        OnDrag(ped);
    }

    public virtual void OnPointerUp(PointerEventData ped)
    {
        inputVector = Vector3.zero;
        joyStickImage.rectTransform.anchoredPosition = Vector3.zero;
        PlayerBehave._instance.SetDirection(Vector3.zero);
    }

    public float Horizontal()
    {
        if (inputVector.x != 0)
        {
            return inputVector.x;
        }
        else
            return Input.GetAxis("Horizontal");
    }
    public float Vertical()
    {
        if (inputVector.z != 0)
        {
            return inputVector.z;
        }
        else
            return Input.GetAxis("Vertical");
    }

    public virtual void OnPointerDrag(PointerEventData ped)
    {

    }
}
