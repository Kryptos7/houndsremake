using System.Collections;
using System.Collections.Generic;
using UnityEngine;


/// <summary>
/// 기본적으로 모든 적들이 참고할 포지션. 적들은 주인공들의 상태가 아니라 위치만 알면 된다.
/// </summary>
public class GameManager : MonoBehaviour
{
    public List<GameObject> players = new List<GameObject>();
    public static GameManager _instance;

    [SerializeField]
    GameObject _captainObj;

    [SerializeField]
    GameObject _swatObj;

    [SerializeField]
    GameObject _sisterObj;

    [SerializeField]
    GameObject _witchObj;

    private void Awake()
    {
        _instance = this;
    }



    private void Start()
    {
        players.Add(_captainObj);

        if (PublicSet.SwatEntry)
        {
            _swatObj.SetActive(true);
        }

        if (PublicSet.SisterEntry)
        {
            _sisterObj.SetActive(true);
        }

        if (PublicSet.WitchEntry)
        {
            _witchObj.SetActive(true);
        }



        if(_swatObj.activeInHierarchy)
        {
            players.Add(_swatObj);
        }

        if (_sisterObj.activeInHierarchy)
        {
            players.Add(_sisterObj);
        }

        if(_witchObj.activeInHierarchy)
        {
            players.Add(_witchObj);
        }
    }





    public void AddKillPoint(string charName, int point = 10)
    {
        for (int i = 0; i < players.Count; i++)
        {
            if(players[i].name == charName)
            {
                players[i].SendMessage("KillPointAdd", point, SendMessageOptions.DontRequireReceiver);
                break;
            }
        }
    }

}
