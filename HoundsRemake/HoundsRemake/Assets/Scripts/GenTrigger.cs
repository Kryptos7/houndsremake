using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GenTrigger : MonoBehaviour
{
    [SerializeField]
    int SpawnCount = 40;

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag(PublicSet.PlayerTag))
        {
            SpawnManager._instance.CallSpawnEnemy(SpawnCount);
            Destroy(this.gameObject);
        }
    }
}
