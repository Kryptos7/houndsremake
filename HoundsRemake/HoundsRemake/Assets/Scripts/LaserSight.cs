using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaserSight : MonoBehaviour
{
    Transform tr;
    LineRenderer line;
    RaycastHit hit;
    FireCtrl _fire;

    // Start is called before the first frame update
    void Start()
    {
        tr = GetComponent<Transform>();
        line = GetComponent<LineRenderer>();
        line.SetWidth(0.1f, 0.01f);
        _fire = GetComponentInParent<FireCtrl>();
    }

    // Update is called once per frame
    void Update()
    {
        Ray ray = new Ray(tr.position + Vector3.up * 0.02f, tr.forward);
        line.SetPosition(0, tr.InverseTransformPoint(ray.origin));

        if (Physics.Raycast(ray, out hit, 15f))
        {
            if (hit.collider.CompareTag(PublicSet.EnemyTag))
            {
                //Debug.Log("적을 겨냥함");
                _fire.Fire();
            }
            line.SetPosition(1, tr.InverseTransformPoint(hit.point));
        }
        else
        {
            line.SetPosition(1, tr.InverseTransformPoint(ray.GetPoint(15f)));
        }
    }
}
