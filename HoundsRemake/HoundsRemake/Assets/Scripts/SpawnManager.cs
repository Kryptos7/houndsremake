using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnManager : MonoBehaviour
{
    public static SpawnManager _instance;

    [SerializeField]
    GameObject[] _enemyPrefabs;

    [SerializeField]
    GameObject[] _genPoints;

    
    private void Awake()
    {
        _instance = this;
        ReFindGenPoint();
    }



    public void ReFindGenPoint()
    {
        _genPoints = GameObject.FindGameObjectsWithTag(PublicSet.GenPointTag);
    }


    /// <summary>
    /// 일단은 랜덤하게, 관리는 나중에 하든가 하자.
    /// </summary>
    /// <param name="Count"></param>
    public void CallSpawnEnemy(int Count = 40)
    {
        StartCoroutine(EnmeySpawn(Count));
    }


    IEnumerator EnmeySpawn(int count)
    {
        while(true)
        {
            yield return new WaitForSeconds(1f);
            count--;

            Vector3 playerPos = PlayerBehave._instance.transform.position;

            List<GameObject> posObjs = new List<GameObject>();

            for (int i = 0; i < _genPoints.Length; i++)
            {
                float dist = Vector3.Distance(playerPos, _genPoints[i].transform.position);
                if(dist > 15f && dist < 20f)
                {
                    posObjs.Add(_genPoints[i]);
                }
            }


            int selectPos = Random.Range(0, posObjs.Count);

            int selectEnemy = Random.Range(0, _enemyPrefabs.Length);

            Instantiate(_enemyPrefabs[selectEnemy], posObjs[selectPos].transform.position, Quaternion.identity);


            if (count < 0)
                break;
        }
    }

}
