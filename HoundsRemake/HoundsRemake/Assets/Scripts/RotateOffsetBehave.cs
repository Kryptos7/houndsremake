using System.Collections;
using System.Collections.Generic;
using UnityEngine;


//주인공 기점으로 회전오프셋만큼 플러스를 시켜준다. 
public class RotateOffsetBehave : MonoBehaviour
{
    Vector3 offsetPlus;

    public static RotateOffsetBehave _instance;

    private void Awake()
    {
        _instance = this;
    }

    public void SetRotationOffset(Vector3 offset)
    {
        offsetPlus = offset;
    }

    public Vector3 GetRotationOffSet()
    {
        return this.transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        this.transform.position = PlayerBehave._instance.transform.position + offsetPlus;
    }
}
