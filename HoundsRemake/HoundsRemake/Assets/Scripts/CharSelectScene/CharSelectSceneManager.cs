using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class CharSelectSceneManager : MonoBehaviour
{
    [SerializeField]
    GameObject _swatSelectedMark;

    [SerializeField]
    GameObject _sisterSelectedMark;

    [SerializeField]
    GameObject _witchSelectedMark;

    [SerializeField]
    GameObject _startButton;


    [SerializeField]
    int SelectedCount = 0;


    public void SwatSelect()
    {
        if(_swatSelectedMark.activeInHierarchy == false)
        {
            
            if(SelectedCount >=2 )
            {
                Debug.Log("더 이상 선택할수 없음");
            }
            else
            {
                _swatSelectedMark.SetActive(true);
                SelectedCount++;
                PublicSet.SwatEntry = true;

                if(SelectedCount == 2)
                {
                    _startButton.SetActive(true);
                }
            }
        }
        else
        {
            _swatSelectedMark.SetActive(false);
            SelectedCount--;
            PublicSet.SwatEntry = false;
            _startButton.SetActive(false);
        }
    }


    public void SisterSelect()
    {
        if (_sisterSelectedMark.activeInHierarchy == false)
        {
            if (SelectedCount >= 2)
            {
                Debug.Log("더 이상 선택할수 없음");
            }
            else
            {
                _sisterSelectedMark.SetActive(true);
                SelectedCount++;
                PublicSet.SisterEntry = true;

                if (SelectedCount == 2)
                {
                    _startButton.SetActive(true);
                }
            }
        }
        else
        {
            _sisterSelectedMark.SetActive(false);
            SelectedCount--;
            PublicSet.SisterEntry = false;
            _startButton.SetActive(false);
        }
    }


    public void WitchSelect()
    {
        if (_witchSelectedMark.activeInHierarchy == false)
        {
            if (SelectedCount >= 2)
            {
                Debug.Log("더 이상 선택할수 없음");
            }
            else
            {
                _witchSelectedMark.SetActive(true);
                SelectedCount++;
                PublicSet.WitchEntry = true;
                if (SelectedCount == 2)
                {
                    _startButton.SetActive(true);
                }
            }
        }
        else
        {
            _witchSelectedMark.SetActive(false);
            SelectedCount--;
            PublicSet.WitchEntry = false;
            _startButton.SetActive(false);
        }
    }


    public void StartButtonClick()
    {
        SceneManager.LoadScene("Demo_URP");

    }

    
    
}
