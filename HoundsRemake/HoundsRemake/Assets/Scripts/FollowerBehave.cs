using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UI;


public class FollowerBehave : MonoBehaviour
{
    Transform tr;
    Transform mainPlayer;
    NavMeshAgent nvAgent;
    Animator _anim;
    Transform _target;

    [SerializeField]
    Transform ThrowReleasePoint;

    float stopDist = 3f;
    int _killPoint = 0;
    int _hp = 30;
    readonly int MAX_HP = 50;


    ///죽었을때, 주인공이 근처에 오면 그래프를 띄워주고, 5초후 반피를 갖고 되살아난다. 벗어나면 그래프는 사라지고 초기화된다.
    float reviveTime = 0f;
    readonly float REVIVE_TIME = 5f;

    /// <summary>
    /// 50이 모이면 던질수 있다.
    /// </summary>
    readonly int THROW_ACTIVE_POINT = 20;

    /// <summary>
    /// 던지기 모션을 하는동안 움직이는 것을 방지하기 위해서.
    /// </summary>
    float throwStateTime = 0f;

    [SerializeField]
    GameObject _showHelp;

    [SerializeField]
    Image _reviveGraph;

    [SerializeField]
    GameObject _throwObj;


    [SerializeField]
    FollowHPGaugeBehave _hpGauge;


    public enum FollowerType
    {
        Sister,
        Witch,
        Swat,
    }

    public enum FollowerState
    {
        idle,   //사주경계 모드로
        trace,  //주인공 따라가기 모드
        attack, //idle 모드에 통합할까 하다가 별도로 나눴다.
        Throw,  
        die
    }

    public bool isDie = false;
    public FollowerState _state = FollowerState.idle;

    /// <summary>
    /// 외부에서 지정해주도록 한다. 프리팹 형태이며, 중복되지 않는다.
    /// </summary>
    public FollowerType _type;


    private void Start()
    {
        _anim = GetComponent<Animator>();
        tr = this.GetComponent<Transform>();
        mainPlayer = PlayerBehave._instance.transform;
        nvAgent = this.GetComponent<NavMeshAgent>();
        ThrowReleasePoint = transform.Find("ThrowReleasePoint");

        nvAgent.destination = mainPlayer.position;

        StartCoroutine(CheckFollowerState());
        StartCoroutine(CheckAction());

        MeshRenderer[] rederers = GetComponentsInChildren<MeshRenderer>();

        for (int i = 0; i < rederers.Length; i++)
        {
            rederers[i].receiveShadows = false;
        }
    }



    float FindNearestEnemy()
    {
        float minDist = 9999f;

        Collider[] colliders = Physics.OverlapSphere(this.transform.position, 10f);

        foreach (Collider hit in colliders)
        {
            if (hit.CompareTag(PublicSet.EnemyTag))
            {
                float tempDistance = Vector3.Distance(hit.transform.position,
                    transform.position);

                if (tempDistance < minDist)
                {
                    minDist = tempDistance;
                    _target = hit.transform;
                }
            }
        }

        return minDist;
    }


    public void AddDamage(int damage)
    {
        _hp -= damage;

        ///Update에서 계산하도록 한다.
        //_hpGauge.SetGauge((float)_hp / (float)MAX_HP);

        if (_hp <= 0 && isDie == false)
        {
            isDie = true;
            _state = FollowerState.die;
            int dieAninSelet = Random.Range(1, 3);
            _anim.SetTrigger("Die" + dieAninSelet);
            StopAllCoroutines();
            nvAgent.isStopped = true;
            nvAgent.enabled = false;
            //this.GetComponent<Collider>().enabled = false;

            this.tr.tag = PublicSet.DeadBody;
            return;
        }
    }




    public void AddHeal(int healPoint)
    {
        if (isDie == false)
        {
            _hp += healPoint;

            if (_hp > MAX_HP)
                _hp = MAX_HP;
        }
    }



    /// <summary>
    /// 죽었을시, 주인공이 근처로 다가오면 일정시간 쿨타임후 되살아난다.
    /// </summary>
    private void Update()
    {
        _hpGauge.SetGauge((float)_hp / (float)MAX_HP);
        if (isDie)
        {
            ///우선은 비활성화. 이건 5월 1일 ~ 5월 2일에 작업하도록 하자.
            
            if (Vector3.Distance(this.tr.position, mainPlayer.position) < 2f)
            {
                _showHelp.SetActive(false);
                _reviveGraph.gameObject.SetActive(true);
                ///그래프를 띄워주고, 5초후 반피를 갖고 되살아난다. 벗어나면 그래프는 사라지고 초기화된다.
                reviveTime += Time.deltaTime;

                ///5초니까 1/5로 하면 된다.
                _reviveGraph.fillAmount = reviveTime / REVIVE_TIME;
                
                if (reviveTime > REVIVE_TIME)
                {
                    isDie = false;
                    _hp = 50;
                    nvAgent.enabled = true;
                    nvAgent.isStopped = true;
                    _anim.SetFloat("Forward", 0);
                    _anim.SetTrigger("Revive");
                    _reviveGraph.gameObject.SetActive(false);
                    this.tr.tag = PublicSet.PlayerTag;
                    StartCoroutine(CheckFollowerState());
                    StartCoroutine(CheckAction());
                }
            }
            else
            {
                _reviveGraph.gameObject.SetActive(false);
                _showHelp.SetActive(true);
                reviveTime = 0;
            }
        }
    }


    /// <summary>
    /// 프레임당 업데이트 하면 개판나니 0.1초마다 하도록 한다.
    /// </summary>
    /// <returns></returns>
    IEnumerator CheckFollowerState()
    {
        while (isDie == false )
        {
            yield return new WaitForSeconds(0.2f);

            if (throwStateTime > 0)
                continue;

            float dist = Vector3.Distance(mainPlayer.position, tr.position);

            if (dist < stopDist)
            {
                _state = FollowerState.idle;

                if (FindNearestEnemy() < 10)
                {
                    _state = FollowerState.attack;
                }
            }
            else
            {
                _state = FollowerState.trace;
            }
        }

    }






    IEnumerator CheckAction()
    {
        while(isDie == false)
        {
            switch(_state)
            {
                case FollowerState.idle:
                    {
                        nvAgent.isStopped = true;
                        _anim.SetFloat("Forward", 0);
                    }
                    break;

                case FollowerState.trace:
                    {
                        nvAgent.destination = mainPlayer.position;
                        nvAgent.isStopped = false;
                        _anim.SetFloat("Forward", nvAgent.velocity.magnitude);
                    }
                    break;

                case FollowerState.attack:
                    {
                        _anim.SetFloat("Forward", 0);
                        nvAgent.isStopped = true;
                        var targetRotation = Quaternion.LookRotation(_target.transform.position - transform.position);
                        transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.Euler(new Vector3(0, targetRotation.eulerAngles.y, 0)), 10f * Time.deltaTime);
                    }
                    break;

                case FollowerState.Throw:
                    {
                        throwStateTime -= Time.deltaTime;
                        _anim.SetFloat("Forward", 0);
                        nvAgent.isStopped = true;
                        if (throwStateTime < 0)
                        {
                            _state = FollowerState.idle;
                        }
                    }
                    break;
            }

            yield return null;
        }
    }

    
    /// <summary>
    /// 공격 시작전 ThrowAction이 가능한지 먼저 판단하도록 한다.
    /// </summary>
    /// <returns></returns>
    public bool CheckThrowAction()
    {
        ///시스터는 공격할때가 아니라 차면 즉시 공격을 진행하도록 한다.
        if(_type != FollowerType.Sister)
        {
            if (FindNearestEnemy() < 10)
            {
                ///보간없이 바로 확 돌아보도록 한다.
                var targetRotation = Quaternion.LookRotation(_target.transform.position - transform.position);
                transform.rotation = targetRotation;

                if (_killPoint >= THROW_ACTIVE_POINT)
                {
                    _killPoint -= THROW_ACTIVE_POINT;
                    throwStateTime = 1f;
                    _anim.SetTrigger("Throw");
                    _state = FollowerState.Throw;
                    return true;
                }
            }
        }
        return false;
    }


    /// <summary>
    /// 애니메이션에서 호출하는 이벤트. 시스터는 이 이벤트를 받지 않는다. 
    /// </summary>
    public void ThrowObj()
    {
        GameObject tempobj = Instantiate(_throwObj, ThrowReleasePoint.position, Quaternion.identity);
        Rigidbody rb = tempobj.GetComponent<Rigidbody>();
        //Debug.Log((ThrowReleasePoint.position - (this.tr.position)).normalized);
        rb.AddForce((ThrowReleasePoint.position - (this.tr.position)).normalized * 10f, ForceMode.Impulse);
    }



    ///힐링팩터를 설치한다.
    IEnumerator MakeHealFactor()
    {
        yield return new WaitForSeconds(0.5f);
        Instantiate(_throwObj,tr.position+new Vector3(0,0.1f,0), Quaternion.Euler(new Vector3(-90f,0,0)));
    }


    public void KillPointAdd(int killPoint)
    {
        _killPoint += killPoint;

        if (_killPoint >= THROW_ACTIVE_POINT)
        {
            ///수녀는 즉시 장치를 설치하도록 한다.
            if (_type == FollowerType.Sister)
            {
                _killPoint -= THROW_ACTIVE_POINT;
                throwStateTime = 1f;
                ///시스터는 장치를 '던지지' 않는다. 제저리에 설치하도록 한다.
                _anim.SetTrigger("Throw");
                StartCoroutine(MakeHealFactor());
                _state = FollowerState.Throw;
                ////힐링팩터 설치를 진행하도록 한다.
            }
            //ThrowAction();
            ///테스트코드. 버튼을 눌러서 활성화되도록 바꿔야 함
            /*
            throwStateTime = 1f;
            _anim.SetTrigger("Throw");
            _state = FollowerState.Throw;
            */
        }
    }
}
