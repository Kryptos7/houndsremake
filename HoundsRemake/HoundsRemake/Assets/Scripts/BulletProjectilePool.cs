using System.Collections;
using System.Collections.Generic;
using UnityEngine;


/// <summary>
/// 투사체 형태의 메모리 풀. 일단은 샷건에서만 쓰일 것이다.
/// </summary>
public class BulletProjectilePool : MonoBehaviour
{
    public static BulletProjectilePool _instance;

    [Header("Bullet Impact Info")]
    public GameObject bulletHitPrefab;
    int impactPoolSize = 30;

    GameObject[] impactPool;
    int currentPoolIndex;

	void Awake()
	{
		if (_instance != null && _instance != this)
		{
			Destroy(gameObject);
			return;
		}
		else
			_instance = this;

		impactPool = new GameObject[impactPoolSize];
		for (int i = 0; i < impactPoolSize; i++)
		{
			impactPool[i] = Instantiate(bulletHitPrefab, _instance.transform) as GameObject;
			impactPool[i].SetActive(false);
		}
	}


	public GameObject GetBulletProjectile()
    {
        for (int i = 0; i < impactPool.Length; i++)
        {
			if (impactPool[i].activeInHierarchy == false)
				return impactPool[i];

		}
		return null;
    }

}
