using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletProjectileBehave : MonoBehaviour
{
	[Header("Movement")]
	float speed = 20f;

	[Header("Life Settings")]
	public float lifeTime = 2f;

	Rigidbody projectileRigidbody;


    private void Start()
    {
		projectileRigidbody = GetComponent<Rigidbody>();
	}

    void OnEnable()
	{
		lifeTime = 2f;
		Invoke("RemoveProjectile", lifeTime);
	}


	void Update()
	{
		Vector3 movement = transform.forward * speed * Time.deltaTime;
		projectileRigidbody.MovePosition(transform.position + movement);
	}

	void OnTriggerEnter(Collider theCollider)
	{
		if (theCollider.CompareTag(PublicSet.EnemyTag) || 
			theCollider.CompareTag(PublicSet.GroundTag))
			RemoveProjectile();
	}

	void RemoveProjectile()
	{
		this.gameObject.SetActive(false);
		//Destroy(gameObject);
	}
}
