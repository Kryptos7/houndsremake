using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GrenadeBehave : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        ///5m 내의 적들을 쓸어버리겠다.
        Collider[] colliders = Physics.OverlapSphere(this.transform.position, 5f);

        foreach (Collider hit in colliders)
        {
            if (hit.CompareTag("Enemy"))
            {
                object[] _params = new object[2];
                _params[0] = null;
                _params[1] = 120;    ///일반 잡몹들은 한방에 쓸어버린다.

                hit.SendMessage("AddDamage", _params, SendMessageOptions.DontRequireReceiver);
                ///플래쉬뱅만큼은 아니지만 1초간의 짧은 스턴을 준다.
                //hit.SendMessage("ForceStun", 1f, SendMessageOptions.DontRequireReceiver);
            }
        }
        //Destroy(this.gameObject);
    }
}
