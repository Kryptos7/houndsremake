using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealingFactorBehave : MonoBehaviour
{
    /// <summary>
    /// 1초에 한번씩 힐을 해준다.
    /// </summary>
    float healTimeTick = 1f;

    float destroyTime = 7f;

    [SerializeField]
    ParticleSystem[] particles;

    private void Start()
    {
        ///7초후 폭파된다.
    }


    private void Update()
    {
        destroyTime -= Time.deltaTime;

        if (destroyTime < 0)
        {
            for (int i = 0; i < particles.Length; i++)
            {
                particles[i].loop = false;
            }
            return;
        }
            


        healTimeTick -= Time.deltaTime;

        if (healTimeTick < 0)
        {
            healTimeTick = 1f;

            Collider[] colliders = Physics.OverlapSphere(this.transform.position, 4f);

            foreach (Collider hit in colliders)
            {
                if (hit.CompareTag(PublicSet.PlayerTag))
                {
                    //hit.SendMessage("AddDamage", _params, SendMessageOptions.DontRequireReceiver);
                    hit.SendMessage("AddHeal", 15);
                }
            }
        }
    }
}
