using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ThrowObjectBehave : MonoBehaviour
{
    [SerializeField]
    GameObject _molotovEff;

    [SerializeField]
    GameObject _flashBangEff;

    [SerializeField]
    GameObject _grenadeEff;

    bool _isDetonated = false;

    [SerializeField]
    public enum ThrowType
    {
        Grenade,
        Molotov,
        FlashBang
    }

    public ThrowType _type;

    private void OnCollisionEnter(Collision collision)
    {
        if (_isDetonated)
            return;


        switch (_type)
        {
            case ThrowType.Grenade:
                {
                    if(collision.transform.CompareTag(PublicSet.EnemyTag) || 
                        collision.transform.CompareTag(PublicSet.GroundTag))
                    {
                        Destroy(this.gameObject);
                        Instantiate(_grenadeEff, this.transform.position, Quaternion.identity);
                        _isDetonated = true;
                        CameraManager._instance.ShakeCam(0.5f);
                    }
                }
                break;


            case ThrowType.FlashBang:
                {
                    if (collision.transform.CompareTag(PublicSet.EnemyTag) || 
                        collision.transform.CompareTag(PublicSet.GroundTag))
                    {
                        Destroy(this.gameObject);
                        Instantiate(_flashBangEff, this.transform.position, Quaternion.identity);
                        _isDetonated = true;
                    }
                }
                break;


            ///ȭ������ �ٴڿ� �������߸� ȿ���� �����Ѵ�.
            case ThrowType.Molotov:
                {
                    if (collision.transform.CompareTag(PublicSet.GroundTag))
                    {
                        Destroy(this.gameObject);
                        Instantiate(_molotovEff, this.transform.position, Quaternion.identity);
                        _isDetonated = true;
                    }
                }
                break;
        }
    }
}
