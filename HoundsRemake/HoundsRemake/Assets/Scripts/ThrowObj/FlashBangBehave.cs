using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlashBangBehave : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        ///7m ���� ������ �˴� ���Ϳ� �ɸ���.
        Collider[] colliders = Physics.OverlapSphere(this.transform.position, 7f);

        foreach (Collider hit in colliders)
        {
            if (hit.CompareTag(PublicSet.EnemyTag))
            {
                hit.SendMessage("ForceStun", 5f, SendMessageOptions.DontRequireReceiver);
            }
        }

        //Destroy(this.gameObject);
    }
}
