using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MolotovBehave : MonoBehaviour
{
    /// <summary>
    /// 1초에 한번씩 대미지를 준다.
    /// </summary>
    float damageTimeTick = 1f;

    private void Start()
    {
        ///7초후 폭파된다.
        //Destroy(this.gameObject, 7f);
    }

    private void Update()
    {
        damageTimeTick -= Time.deltaTime;

        if(damageTimeTick < 0)
        {
            damageTimeTick = 1f;

            Collider[] colliders = Physics.OverlapSphere(this.transform.position, 4f);

            foreach (Collider hit in colliders)
            {
                if (hit.CompareTag(PublicSet.EnemyTag))
                {
                    object[] _params = new object[2];
                    _params[0] = null;
                    _params[1] = 20;

                    hit.SendMessage("AddDamage", _params, SendMessageOptions.DontRequireReceiver);
                }
            }
        }
    }
}
