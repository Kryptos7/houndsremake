using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireCtrl_Pistol : FireCtrl
{
    [SerializeField]
    ParticleSystem _muzzleEff;

    protected override void Start()
    {
        base.Start();
        referencedCoolTime = 0.5f;
        _bulletLine.damage = 15;
    }

    public override void Fire()
    {
        if (_coolTime < 0)
        {
            _bulletLine.CallBulletLine();
            _coolTime = referencedCoolTime;
            _muzzleEff.Play();
        }
    }
}
