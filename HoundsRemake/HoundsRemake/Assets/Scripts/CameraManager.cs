using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraManager : MonoBehaviour
{
    [SerializeField]
    Transform _target;

    float smoothing = 10f;    ///작으면 작을수록 부드럽다. 7 정도면 빠르면서도 적당히 부드럽다.
    [SerializeField]
    Vector3 offSet;

    public Transform Target { get => _target; set => _target = value; }
    public static CameraManager _instance;

    [SerializeField]
    Transform _camObj;

    Vector3 _originalCamPos;

    float shakeTime = 0f;
    float shakeAmount = 0.05f;

    void Awake()
    {
        _instance = this;
        
        //_originalCameraSize = _mainCamera.orthographicSize;
        _camObj = transform.Find("Main Camera");

        _originalCamPos = _camObj.localPosition;
        
    }

    private void Start()
    {
        offSet = new Vector3(-4.16f, 9.42f, 6.9f);//this.transform.position;
        StartCoroutine(ShakeCamWithTime());
    }



    public void ShakeCam(float time = 0.1f)
    {
        if(shakeTime < 0)
        {
            shakeTime = time;
        }
        else
        {
            shakeTime += time;
        }
        
    }

    IEnumerator ShakeCamWithTime()
    {
        while (true)
        {
            yield return null;
            shakeTime -= Time.deltaTime;
            if (shakeTime < 0)
            {
                _camObj.localPosition = _originalCamPos;
            }
            else
            {
                _camObj.localPosition = Random.insideUnitSphere * shakeAmount + _originalCamPos;
            }
        }
    }


    private void Update()
    {
        if (_target != null)
        {
            Vector3 targetCamPos = _target.position + offSet;
            //transform.position = targetCamPos;//Vector3.Lerp(transform.position, targetCamPos, smoothing * Time.deltaTime);
            transform.position = Vector3.Lerp(transform.position, targetCamPos, smoothing * Time.deltaTime);
        }
    }

}
