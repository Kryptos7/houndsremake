using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerBehave : MonoBehaviour
{
    [Header("Camera")]
    public Camera mainCamera;

    float speed = 3f;
    public LayerMask whatIsGround;

    [Header("Animation")]
    public Animator playerAnimator;

    Rigidbody playerRigidbody;

    [SerializeField]
    Transform ThrowReleasePoint;

    public static PlayerBehave _instance;

    bool isDie;

    Vector3 inputDirection;

    int _killPoint = 10000;

    int _hp = 100;

    /// <summary>
    /// 50이 모이면 던질수 있다.
    /// </summary>
    readonly int THROW_ACTIVE_POINT = 100;
    readonly int MAX_HP = 100;

    /// <summary>
    /// 던지기 모션을 하는동안 움직이는 것을 방지하기 위해서.
    /// </summary>
    float throwStateTime = 0f;

    [SerializeField]
    GameObject _throwObj;

    [SerializeField]
    FollowHPGaugeBehave _hpGauge;


    void Awake()
    {
        ThrowReleasePoint = transform.Find("ThrowReleasePoint");
        playerRigidbody = GetComponent<Rigidbody>();
        _instance = this;
    }


    public void SetDirection(Vector3 direction)
    {
        inputDirection = direction;
    }


    public void AddHeal(int healPoint)
    {
        if(isDie == false)
        {
            _hp += healPoint;

            if (_hp > MAX_HP)
                _hp = MAX_HP;
        }
    }


    public void AddDamage(int damage)
    {
        _hp -= damage;

        ///Update에서 계산하도록 한다.
        //_hpGauge.SetGauge((float)_hp / (float)MAX_HP);

        if (_hp <= 0 && isDie == false)
        {
            isDie = true;
            int dieAninSelet = Random.Range(0, 4);

            //playerAnimator.SetTrigger("die");
            StopAllCoroutines();
            this.GetComponent<Collider>().enabled = false;
            this.GetComponent<CapsuleCollider>().enabled = false;
            this.transform.tag = PublicSet.DeadBody;
            return;
        }
    }


    /// <summary>
    /// 외부 버튼으로 '던지기'를 호출한다. 조건도 여기에 달릴 예정임.
    /// </summary>
    public void CallThrow()
    {
        if (isDie)
            return;

        if (_killPoint >= THROW_ACTIVE_POINT)
        {
            _killPoint -= THROW_ACTIVE_POINT;
            throwStateTime = 1f;
            playerAnimator.SetTrigger("Throw");
        }
    }

    public void ThrowObj()
    {
        GameObject tempobj = Instantiate(_throwObj, ThrowReleasePoint.position, Quaternion.identity);
        Rigidbody rb = tempobj.GetComponent<Rigidbody>();
        //Debug.Log((ThrowReleasePoint.position - (this.tr.position)).normalized);
        rb.AddForce((ThrowReleasePoint.position - (this.transform.position)).normalized * 10f, ForceMode.Impulse);
    }



    public void Update()
    {
        _hpGauge.SetGauge((float)_hp / (float)MAX_HP);
    }


    void FixedUpdate()
    {
        if (isDie)
            return;

        if(throwStateTime > 0)
        {
            throwStateTime -= Time.deltaTime;
            return;
        }

        
        //Arrow Key Input
        float h = Input.GetAxis("Horizontal");
        float v = Input.GetAxis("Vertical");

        Vector3 inputDirection = new Vector3(h, 0, v);
        

        //Camera Direction
        var cameraForward = mainCamera.transform.forward;
        var cameraRight = mainCamera.transform.right;

        cameraForward.y = 0f;
        cameraRight.y = 0f;

        //Try not to use var for roadshows or learning code
        Vector3 desiredDirection = cameraForward * inputDirection.z + cameraRight * inputDirection.x;

        //Why not just pass the vector instead of breaking it up only to remake it on the other side?
        MoveThePlayer(desiredDirection);
        TurnThePlayer();
        AnimateThePlayer(desiredDirection);
    }


    void MoveThePlayer(Vector3 desiredDirection)
    {
        Vector3 movement = new Vector3(desiredDirection.x, 0f, desiredDirection.z);
        movement = movement.normalized * speed * Time.deltaTime;

        playerRigidbody.MovePosition(transform.position + movement);
    }

    void TurnThePlayer()
    {

        //Ray ray = mainCamera.ScreenPointToRay(Input.mousePosition);
        //Ray ray = mainCamera.ScreenPointToRay(RotateOffsetBehave._instance.GetRotationOffSet());
        //RaycastHit hit;

        //if (Physics.Raycast(ray, out hit, whatIsGround))
        {
            Vector3 playerToMouse = transform.position - RotateOffsetBehave._instance.GetRotationOffSet() ;
            playerToMouse.y = 0f;
            playerToMouse.Normalize();

            if(playerToMouse != Vector3.zero)
            {
                Quaternion newRotation = Quaternion.LookRotation(playerToMouse);
                playerRigidbody.MoveRotation(newRotation);
            }
        }
    }

    void AnimateThePlayer(Vector3 desiredDirection)
    {
        if (!playerAnimator)
            return;

        Vector3 movement = new Vector3(desiredDirection.x, 0f, desiredDirection.z);
        float forw = Vector3.Dot(movement, transform.forward);
        float stra = Vector3.Dot(movement, transform.right);

        playerAnimator.SetFloat("Forward", forw);
        playerAnimator.SetFloat("Strafe", stra);
    }

    /// <summary>
    /// 주인공은 특수스킬 버튼을 눌렀을때 쏘도록 한다.
    /// </summary>
    /// <returns></returns>
    public bool ThrowAction()
    {
        if (isDie)
            return false;

        if (_killPoint >= 0)
        {
            _killPoint -= 0;
            throwStateTime = 1f;
            playerAnimator.SetTrigger("Throw");
            return true;
        }
        return false;
    }

    public void KillPointAdd(int killPoint)
    {
        _killPoint += killPoint;
    }
}
