using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CamPivotBehave : MonoBehaviour
{
    Vector3 offsetPlus;
    public static CamPivotBehave _instance;

    private void Awake()
    {
        _instance = this;
    }

    public void SetRotationOffset(Vector3 offset)
    {
        offsetPlus = offset;
    }

    public Vector3 GetRotationOffSet()
    {
        return this.transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        this.transform.position = PlayerBehave._instance.transform.position - offsetPlus;
    }
}
