using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireCtrl_Shotgun : FireCtrl
{
    BulletLine[] _bulletLineArray;
    [SerializeField]
    ParticleSystem _muzzleEff;

    protected override void Start()
    {
        referencedCoolTime = _coolTime;
        firePos = this.transform;
        _bulletLineArray = GetComponentsInChildren<BulletLine>();

        for (int i = 0; i < _bulletLineArray.Length; i++)
        {
            _bulletLineArray[i].damage = 10;
        }
    }



    public override void Fire()
    {
        if (_coolTime < 0)
        {
            for (int i = 0; i < _bulletLineArray.Length; i++)
            {
                _bulletLineArray[i].CallBulletLine();
            }
            _coolTime = referencedCoolTime;
            _muzzleEff.Play();
        }
    }
}
