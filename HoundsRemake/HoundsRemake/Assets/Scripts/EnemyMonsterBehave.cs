using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyMonsterBehave : EnemyTyrantBehave
{

    protected readonly float WAIT_TIME = 1f;
    [SerializeField]
    protected float waitTime = 1f;

    protected override void Start()
    {
        base.Start();
    }


    /// <summary>
    /// 타일런트의 스턴타임은 1초다.
    /// </summary>
    /// <param name="time"></param>
    public override void ForceStun(float time)
    {
        stunTime = 1;
        _state = EnemyState.stun;
        _anim.SetTrigger("stun");
    }

    protected override IEnumerator CheckFollowerState()
    {
        while (isDie == false)
        {
            yield return new WaitForSeconds(0.5f);


            if (_state != EnemyState.stun )
            {
                ///trace 상태로 들어가면 주기적으로 가장 가까운 적을 찾는다.
                float minDist = FindNearestPlayerDist();
                //Debug.Log(minDist);

                if (minDist <= attackRange)
                {
                    nvAgent.isStopped = true;
                    _state = EnemyState.attack;
                }
                else
                {
                    nvAgent.isStopped = false;
                    _state = EnemyState.trace;
                }
            }
        }
    }

    public void Spit()
    {
        Debug.Log("온다");
    }

    protected override IEnumerator CheckAction()
    {
        while (isDie == false)
        {
            switch (_state)
            {
                case EnemyState.idle:
                    {
                        nvAgent.isStopped = true;
                    }
                    break;


                ///상태전환용 FSM 
                case EnemyState.detected:
                    {
                        _state = EnemyState.trace;
                    }
                    break;

                case EnemyState.trace:
                    {
                        nvAgent.isStopped = false;
                        nvAgent.destination = _target.transform.position;
                        _anim.SetBool("chase", true);
                    }
                    break;
                case EnemyState.attack:
                    {
                        var targetRotation = Quaternion.LookRotation(_target.transform.position - transform.position);
                        transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.Euler(new Vector3(0, targetRotation.eulerAngles.y, 0)), 1f * Time.deltaTime);
                        nvAgent.isStopped = true;
                        _anim.SetTrigger("attack");
                    }
                    break;


                case EnemyState.stun:
                    {
                        //FindNearestPlayerDist();
                        nvAgent.isStopped = true;
                        stunTime -= Time.deltaTime;
                        if (stunTime < 0)
                        {
                            FindNearestPlayerDist();
                            _state = EnemyState.trace;
                        }
                    }
                    break;
            }
            yield return null;
        }
    }

    public override void AddDamage(object[] param)
    {
        _hp -= (int)param[1];

        if (_hp <= 0 && isDie == false)
        {
            isDie = true;
            _state = EnemyState.die;

            _anim.SetTrigger("die");
            StopAllCoroutines();
            nvAgent.isStopped = true;
            nvAgent.enabled = false;
            this.GetComponent<Collider>().enabled = false;
            Destroy(this.gameObject, 4f);
            ///param이 null이라는 것은 불특정한 설치물 혹은 폭발물에 의해 죽었다는 얘기다.
            if (param[0] != null)
            {
                GameManager._instance.AddKillPoint(param[0].ToString());
            }

            this.tr.tag = PublicSet.DeadBody;
            return;
        }
    }
}
