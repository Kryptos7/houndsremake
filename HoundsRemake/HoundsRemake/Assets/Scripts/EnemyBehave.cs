using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;


/// <summary>
/// 1. 모든 좀비는 전지적이다. 단지 행동만 '나쁜놈'의 역할을 대신해줄 뿐이다.
/// 2. 
/// </summary>
public class EnemyBehave : MonoBehaviour
{
    protected Transform tr;
    [SerializeField]
    protected GameObject _target;
    protected NavMeshAgent nvAgent;
    protected Animator _anim;

    /// <summary>
    /// 일정거리 미만이 되면 양쪽 충돌박스의 문제때문에 괴랄하게 움직이는 문제가 발생한다. 
    /// 1.6f, StopDist0.9 에서 아슬아슬하게 해당문제가 발생하지 않음.
    /// ver2 지나치게 붙는 감이 있어서 조금 늘려보았다. 
    /// </summary>
    [SerializeField]
    protected float attackRange = 2f;

    /// <summary>
    /// 10미터 안에 들어왔으면 쫓아가도록 한다. 이것은 각 객체마다 다를수 있다.
    /// ver2. 그런거 없고, 생성과 동시에 무조건 따라가도록 한다. 
    /// </summary>
    protected float traceDist = 9999;

    public enum EnemyState
    {
        idle,   //사주경계 모드로
        detected,  //주인공을 발견하거나, 공격당했음. 1회성 이벤트이다.
        trace,  //주인공 따라가기 모드
        attack, ///좀비는 밀리 공격이 기본이다.
        stun,
        wait,   //특수 좀비일 경우 공격후 대기시간이 있을수 있다.
        die
    }

    protected bool isDie = false;
    protected bool detected = false;

    /// <summary>
    /// 기본 스턴 타임은 0.2초로 하도록 한다.
    /// </summary>
    protected float stunTime = 0.0f;

    public EnemyState _state = EnemyState.idle;


    /// <summary>
    /// 최대 hp
    /// </summary>
    [SerializeField]
    protected int _hp = 200;


    // Start is called before the first frame update
    virtual protected void Start()
    {
        _anim = GetComponent<Animator>();
        tr = this.GetComponent<Transform>();
        //v_target = PlayerBehave._instance.transform;
        nvAgent = this.GetComponent<NavMeshAgent>();

        //nvAgent.destination = _target.position;

        StartCoroutine(CheckFollowerState());
        StartCoroutine(CheckAction());
    }


    virtual protected float FindNearestPlayerDist()
    {
        float minDist = 9999f;

        for (int i = 0; i < GameManager._instance.players.Count; i++)
        {
            if(GameManager._instance.players[i].CompareTag("DeadBody"))
            {
                continue;
            }

            float dist = Vector3.Distance(GameManager._instance.players[i].transform.position, this.tr.position);
            if (dist < minDist)
            {
                minDist = dist;
                _target = GameManager._instance.players[i];
            }
        }
        return minDist;
    }



    /// <summary>
    /// 플레이어 캐릭터가 2m 이내에 있다면 스턴에 걸리지 않는다.
    /// ver2. 스턴 시스템은 없앤다.
    /// </summary>
    /// <returns></returns>
    virtual protected bool CanStun()
    {
        return false;

        /*
        float minDist = 9999f;

        for (int i = 0; i < GameManager._instance.players.Count; i++)
        {
            float dist = Vector3.Distance(GameManager._instance.players[i].transform.position, this.tr.position);
            if (dist < minDist)
            {
                minDist = dist;
            }
        }

        if(minDist < 3f)
        {
            Debug.Log("3m 이내라 스턴에 걸리지 않음");
            return false;
        }
        return true;
        */
    }



    /// <summary>
    /// 플래쉬뱅에 의해서 3초간 강제로 스턴되었다. 
    /// </summary>
    virtual public void ForceStun(float time = 3f)
    {
        stunTime = time;
        _state = EnemyState.stun;
        int stunAninSelet = Random.Range(0, 4);
        _anim.SetBool("stun"+ stunAninSelet, true);
    }

    /// <summary>
    /// param[0] : 대미지를 받은 위치
    /// param[1] : 대미지량
    /// </summary>
    /// <param name="param"></param>
    virtual public void AddDamage(object[] param)
    {
        /*
        Debug.Log(param[0]);
        Debug.Log(param[1]);
        */

        _hp -= (int)param[1];

        if(_hp <= 0 && isDie == false)
        {
            isDie = true;
            _state = EnemyState.die;

            int dieAninSelet = Random.Range(0, 4);

            _anim.SetTrigger("die"+dieAninSelet);
            StopAllCoroutines();
            nvAgent.isStopped = true;
            nvAgent.enabled = false;
            this.GetComponent<Collider>().enabled = false;
            Destroy(this.gameObject, 4f);
            ///param이 null이라는 것은 불특정한 설치물 혹은 폭발물에 의해 죽었다는 얘기다.
            if (param[0] != null)
            {
                GameManager._instance.AddKillPoint(param[0].ToString());
            }
            
            this.tr.tag = PublicSet.DeadBody;
            return;
        }


        ///이건 더 이상 쓰지 않는다. 다음에 잠입관련된게 나오거나 하면 모를까...
        /*
        switch (_state)
        {
            case EnemyState.trace:
                {
                    int stunPercent = Random.Range(0, 100);
                    if (stunPercent < 15)
                    {
                        if(CanStun())
                        {
                            stunTime = 0.3f;
                            _state = EnemyState.stun;
                        }
                    }
                }
                break;

            case EnemyState.idle:
                {
                    FindNearestPlayerDist();
                    _state = EnemyState.detected;
                }
                break;
        }
        */
    }


    virtual public void SendDamage()
    {
        if(_target != null)
        {
            _target.SendMessage("AddDamage", 3);
        }
    }




    /// <summary>
    /// 프레임당 업데이트 하면 개판나니 0.5 초마다 하도록 한다.
    /// </summary>
    /// <returns></returns>
    virtual protected IEnumerator CheckFollowerState()
    {
        while (isDie == false)
        {
            yield return new WaitForSeconds(0.5f);

            
            if (FindNearestPlayerDist() > traceDist && _state == EnemyState.idle)
            {
                _state = EnemyState.idle;
            }
            else
            {
                ///한번 trace 상태로 들어가면 다른 턴으로 돌아가지 않음
                if(detected == false)
                {
                    _state = EnemyState.detected;
                    detected = true;
                }
                else
                {
                    if(_state != EnemyState.stun)
                    {
                        ///trace 상태로 들어가면 주기적으로 가장 가까운 적을 찾는다.
                        float minDist = FindNearestPlayerDist();
                        //Debug.Log(minDist);

                        if (minDist <= attackRange)
                        {
                            nvAgent.isStopped = true;
                            _state = EnemyState.attack;
                        }
                        else
                        {
                            nvAgent.isStopped = false;
                            _state = EnemyState.trace;
                        }
                    }
                }
                //_state = EnemyState.trace;
            }
        }
    }

    virtual protected IEnumerator CheckAction()
    {
        while (isDie == false)
        {
            switch (_state)
            {
                case EnemyState.idle:
                    {
                        nvAgent.isStopped = true;
                    }
                    break;


                    ///상태전환용 FSM 
                case EnemyState.detected:
                    {
                        _anim.SetTrigger("detected");
                        _state = EnemyState.trace;
                    }
                    break;

                case EnemyState.trace:
                    {
                        _anim.SetBool("attack", false);
                        nvAgent.isStopped = false;
                        nvAgent.destination = _target.transform.position;
                        _anim.SetBool("chase", true);
                    }
                    break;
                case EnemyState.attack:
                    {
                        var targetRotation = Quaternion.LookRotation(_target.transform.position - transform.position);
                        transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.Euler(new Vector3(0, targetRotation.eulerAngles.y, 0)), 1f * Time.deltaTime);
                        _anim.SetBool("attack", true);
                    }
                    break;

                case EnemyState.stun:
                    {
                        //FindNearestPlayerDist();
                        nvAgent.isStopped = true;
                        stunTime -= Time.deltaTime;
                        if(stunTime < 0)
                        {
                            for (int i = 0; i < 4; i++)
                            {
                                _anim.SetBool("stun"+i, false);
                            }
                            
                            FindNearestPlayerDist();
                            _state = EnemyState.trace;
                        }
                    }
                    break;
            }

            yield return null;
        }
    }



}
