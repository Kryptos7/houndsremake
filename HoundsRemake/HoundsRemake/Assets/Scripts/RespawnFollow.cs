using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RespawnFollow : MonoBehaviour
{
    //자신이 따라가야 될 캐릭터
    Transform _target;

    [SerializeField]


    public static RespawnFollow _instance;

    private void Awake()
    {
        _instance = this;
    }

    private void Start()
    {
        _target = PlayerBehave._instance.transform;
    }


    public void SetSpawnStart()
    {

    }


    public void Update()
    {
        if (_target.gameObject.activeInHierarchy == false)
        {
            this.gameObject.SetActive(false);
            return;
        }

        transform.position = _target.position + new Vector3(0, 0.1f, 0);
    }
}
