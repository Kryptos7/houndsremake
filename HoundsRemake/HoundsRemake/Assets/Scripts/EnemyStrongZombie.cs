using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyStrongZombie : EnemyTyrantBehave
{
    protected override IEnumerator CheckFollowerState()
    {
        while (isDie == false)
        {
            yield return new WaitForSeconds(0.5f);


            if (FindNearestPlayerDist() > traceDist && _state == EnemyState.idle)
            {
                _state = EnemyState.idle;
            }
            else
            {
                ///한번 trace 상태로 들어가면 다른 턴으로 돌아가지 않음
                if (detected == false)
                {
                    _state = EnemyState.detected;
                    detected = true;
                }
                else
                {
                    if (_state != EnemyState.stun)
                    {
                        ///trace 상태로 들어가면 주기적으로 가장 가까운 적을 찾는다.
                        float minDist = FindNearestPlayerDist();
                        //Debug.Log(minDist);

                        if (minDist <= attackRange)
                        {
                            nvAgent.isStopped = true;
                            _state = EnemyState.attack;
                        }
                        else
                        {
                            nvAgent.isStopped = false;
                            _state = EnemyState.trace;
                        }
                    }
                }
                //_state = EnemyState.trace;
            }
        }
    }


    protected override IEnumerator CheckAction()
    {
        while (isDie == false)
        {
            switch (_state)
            {
                case EnemyState.idle:
                    {
                        nvAgent.isStopped = true;
                    }
                    break;


                ///상태전환용 FSM 
                case EnemyState.detected:
                    {
                        _state = EnemyState.trace;
                    }
                    break;

                case EnemyState.trace:
                    {
                        nvAgent.isStopped = false;
                        nvAgent.destination = _target.transform.position;
                        _anim.SetBool("chase", true);
                    }
                    break;
                case EnemyState.attack:
                    {
                        var targetRotation = Quaternion.LookRotation(_target.transform.position - transform.position);
                        transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.Euler(new Vector3(0, targetRotation.eulerAngles.y, 0)), 1f * Time.deltaTime);
                        int select = Random.Range(0, 3);
                        _anim.SetTrigger("attack"+select);
                        _state = EnemyState.wait;
                    }
                    break;

                case EnemyState.wait:
                    {

                    }
                    break;

                case EnemyState.stun:
                    {
                        //FindNearestPlayerDist();
                        nvAgent.isStopped = true;
                        stunTime -= Time.deltaTime;
                        if (stunTime < 0)
                        {
                            FindNearestPlayerDist();
                            _state = EnemyState.trace;
                        }
                    }
                    break;
            }
            yield return null;
        }
    }

}
