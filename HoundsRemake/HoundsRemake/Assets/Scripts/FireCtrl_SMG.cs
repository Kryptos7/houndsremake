using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireCtrl_SMG : FireCtrl
{
    BulletLine[] _bulletLineArray;

    protected override void Start()
    {
        //base.Start();
        firePos = this.transform;
        _bulletLineArray = GetComponentsInChildren<BulletLine>();
        for (int i = 0; i < _bulletLineArray.Length; i++)
        {
            _bulletLineArray[i].damage = 10;
        }
        referencedCoolTime = 0.7f;
    }

    public override void Fire()
    {
        if (_coolTime < 0)
        {
            _coolTime = referencedCoolTime;
            StartCoroutine(SMGFire());
        }
    }

    IEnumerator SMGFire()
    {
        for (int i = 0; i < 10; i++)
        {
            yield return new WaitForSeconds(0.05f);
            //_bulletLine.CallBulletLine();
            _bulletLineArray[Random.Range(0, _bulletLineArray.Length - 1)].CallBulletLine();
            StartCoroutine(ShowMuzzleFlash());

            ///이건 여기에 왜 한번더 붙어있지? 없애버려보자.
            _coolTime = referencedCoolTime;
        }
    }
}
